const request = require('request-promise-native');
const tough = require('tough-cookie');
const cheerio = require('cheerio');

module.exports = (url, {cookies, token}) => new Promise(async (resolve, reject) => {
    const jar = request.jar();

    cookies.map(c => new tough.Cookie({ key: c.key, value: c.value }))
        .forEach(c => jar.setCookie(c, url));

    const pageResponse = await request({
        uri: url,
        jar,
        method: 'POST',
        form: { token }
    });

    const $ = cheerio.load(pageResponse);
    const answer = $("#answer").text();

    return answer ? resolve(answer) : reject(`Forbidden`);
});
