const cheerio = require('cheerio');

/**
 * Parse token equals adpagespeed.js.
 * @param rawToken
 * @returns {string}
 */
const parseToken = rawToken => {
    const replacements = {
        'a': 'z',
        'b': 'y',
        'c': 'x',
        'd': 'w',
        'e': 'v',
        'f': 'u',
        'g': 't',
        'h': 's',
        'i': 'r',
        'j': 'q',
        'k': 'p',
        'l': 'o',
        'm': 'n',
        'n': 'm',
        'o': 'l',
        'p': 'k',
        'q': 'j',
        'r': 'i',
        's': 'h',
        't': 'g',
        'u': 'f',
        'v': 'e',
        'w': 'd',
        'x': 'c',
        'y': 'b',
        'z': 'a'
    };

    let token = rawToken.split('');

    for (let i = 0; i < token.length; i++) {
        token[i] = replacements.hasOwnProperty(token[i]) ? replacements[token[i]] : token[i];
    }

    return token.join('');
};

module.exports = response => {
    const $ = cheerio.load(response.body);

    return parseToken($("#token").val());
};
