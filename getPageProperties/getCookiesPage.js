const Cookie = require('request-cookies').Cookie;

module.exports = response => response.headers['set-cookie']
    .map(cookie => new Cookie(cookie))
    .map(cookie => ( {
        key: cookie.key,
        value: cookie.value
    } ));
