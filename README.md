# Crawler Crawly

## How to use

Firstly you need to have the NodeJS installed in your computer, having installed, just access the folder and install 
the dependencies running:

```
npm install
```

After the all dependencies installed, you just need run:

```
node index.js
```

Done! The result will be shown in your screen.
