const getCookiesPage = require('./getPageProperties/getCookiesPage');
const getTokenPage = require('./getPageProperties/getTokenPage');
const request = require('request-promise-native');

module.exports = pageUrl => new Promise(async resolve => {
    const pageResponse = await request({
        uri: pageUrl,
        method: 'GET',
        resolveWithFullResponse: true
    });

    const cookies = getCookiesPage(pageResponse);
    const token = getTokenPage(pageResponse);

    return resolve({cookies, token});
});
