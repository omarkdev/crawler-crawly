const getPageProperties = require('./getPageProperties');
const findAnswer = require('./findAnswer');

const pageUrl = 'http://applicant-test.us-east-1.elasticbeanstalk.com';

getPageProperties(pageUrl)
    .then(properties => findAnswer(pageUrl, properties))
    .then(answer => console.log(`Result: ${answer}`))
    .catch(err => console.error(`Error: ${err}`));
